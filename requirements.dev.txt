-i https://pypi.org/simple
appnope==0.1.0; sys_platform == 'darwin'
backcall==0.1.0
decorator==4.3.2; python_version >= '2.6'
freezegun==0.3.11
ipython-genutils==0.2.0
ipython==7.2.0
jedi==0.13.2
nose==1.3.7
parso==0.3.3
pexpect==4.6.0; sys_platform != 'win32'
pickleshare==0.7.5
prompt-toolkit==2.0.8; python_version != '3.0.*'
ptyprocess==0.6.0
pycodestyle==2.5.0
pygments==2.3.1
python-dateutil==2.8.0; python_version != '3.0.*'
six==1.12.0; python_version != '3.1.*'
traitlets==4.3.2; python_version >= '2.6'
wcwidth==0.1.7
